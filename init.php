<?php

/*-------------------- none UI --------------------*/

includeInitPHP();
includeInitCSS();
includeInitJS();

$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
$goProfile;

if (STAGE == 'LOCAL') {
	$goProfile = "http://localhost/signup.php" !== $referer && "http://localhost/init.php" !== $referer;
} else if (STAGE == 'ALPHA_PRODUCTION') {
	$goProfile = "http://route-25.com/signup.php" !== $referer && "http://route-25.com/init.php" !== $referer;
}

if ($goProfile) {
	header("Location: ./profile.php");
	exit;
}

include_once 'session.php';
startSession($userstr, $user, $loggedin);

createDefaultCover($user);
updateProfilePic($user);
if (isset($_POST['birthday'])) {
	updateIntro($user);
	header("Location: ./profile.php");
	exit;
}

/*-------------------- UI --------------------*/

$smarty = getSmarty();
$thumb = './upload/profile/' . $user . '_thumb.jpg';
if (!file_exists($thumb)) {
	$thumb = './image/default/default_thumb.jpg';
}
$smarty->assign('thumb', $thumb);
$smarty->display('init/main.tpl');

/*-------------------- functions --------------------*/

function includeInitCSS()
{
	echo "<link rel='stylesheet' href='css/init.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/modal.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/common.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/cropProfile.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/jquery.Jcrop.min.css' type='text/css' />";
	echo "<link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css'/>";
	echo "<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>";
}

function includeInitJS()
{
	echo "<link rel='stylesheet' href='css/profileCrop.css' type='text/css' />";
	echo "<script src='./js/bevel.js'></script>";
	echo "<script src='./js/modal.js'></script>";
	echo "<script src='./js/jquery.Jcrop.min.js'></script>";
	echo "<script src='./js/crop.js'></script>";
	echo "<script src='./js/cropProfile.js'></script>";
}

function includeInitPHP()
{
	include_once 'common.php';
	include_once 'common_smarty.php';
	include_once 'profile-util.php';
}

function createDefaultCover($user)
{
	copy("upload/profile/default_cover.jpg", "upload/profile/" . $user . "_cover.jpg");
}

?>
