<?php /* works.php */

/*-------------------- none UI --------------------*/

// include_once 'session.php';
// startSession($userstr, $user, $loggedin);

// // if (!$loggedin) die();
// if (!$loggedin)
// {
// 	header("Location: ./login.php");
// }


/*-------------------- UI --------------------*/

includeWorksPHP();
includeWorksCSS();
includeWorksJS();

// echo "<div class='ms-main'>";

// showWorks($view, $user);

////printFormForWorks();
// echo "</div>";

// closeHtml();

/*-------------------- functions --------------------*/

function includeWorksPHP()
{
	include_once 'config.php';
	include_once 'common.php';
	include_once 'modal.php';
	include_once 'bevel.php';
	include_once 'like.php';
}

function includeWorksCSS()
{
	echo "<link rel='stylesheet' href='css/works.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/bevel.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/masonry.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/modalPicture.css' type='text/css' />";
}

function includeWorksJS()
{
	echo "<script src='./js/masonry.pkgd.min.js'></script>";
	echo "<script src='./js/masonry.js' ></script>";
	echo "<script src='./js/modal.js' ></script>";
	echo "<script src='./js/modalPicture.js' ></script>";
}

function registerWork($user, $title, $detail)
{
	$saveDir = "./upload/works/$user";
	queryMysql("INSERT INTO pictures(user, title, detail, path, num_good) VALUES('$user', '$title', '$detail', './upload/works/$user', 0)");
	return mysql_insert_id();
}

function unregisterWork($user, $id)
{
	$saveDir = "./upload/works/$user";
	queryMysql("DELETE FROM pictures where id='$id'");
	unlink("./upload/works/" . $user . "/" . $user . "_" . $id . ".jpg");
	return mysql_insert_id();
}

function updateWorks($user) {
	if (isset($_FILES['image']['name']) && isset($_POST['title']) && isset($_POST['upload_work']))
	{
		$title = sanitizeString($_POST['title']);
		$title = preg_replace('/\s\s+/', ' ', $title);
	
		$detail = "";
		if (isset($_POST['detail']))
		{
			$detail = sanitizeString($_POST['detail']);
			$detail = preg_replace('/\s\s+/', ' ', $detail);
		}
		$id = registerWork($user, $title, $detail); 
		saveAsJpeg($_FILES['image']['tmp_name'], $_FILES['image']['type'], "./upload/works/$user", $user . "_" . $id);
	}
	elseif (isset($_POST['id']) && isset($_POST['delete_work']))
	{
		$id = preg_replace('/\s\s+/', ' ', sanitizeString($_POST['id']));
		unregisterWork($user, $id);
	}
}

// function showWorks($view, $user)
// {
// 	$items = getWorksItemList($view, $user);

// 	$smarty = getSmarty();
// 	$smarty->assign('items', $items);
// 	$smarty->display('works/main.tpl');
// }

function getWorksItemList($view, $user)
{
    $pics = queryMysql("SELECT * FROM pictures where user = '$view' order by upload_time desc");

    $num_pics = mysql_num_rows($pics);
    if (!$num_pics)
    {
        echoError("mysql_num_rows returned 0");
        return;
    }

//     echo "<div id='works-content-d' class='ms-container h-centered have-padding have-shadow'>";
////        echo "<div class='bevel-blue' onclick='editProfile(this)'></div>";
	$items = array();
    for($i = 0; $i < $num_pics; $i++)
    {
        $row = mysql_fetch_row($pics);
//         $id = $row[1];
//         $path = $row[5];
//         $title = $row[2];
//         $description = $row[3];
        $path = "$row[5]/$row[0]" . "_" . $row[1] . ".jpg";
//         $num_like = $row[6];
        $isLiked = isLiked($row[1], $user);

        if(empty($row[2]) || !file_exists("$path"))
        {
            echoError("picture not found: $path");
            continue;
        }
        $item = array($path, $row[1], $isLiked, str_replace(array("\r\n", "\r", "\n"), '<br>', $row[2]),
        		str_replace(array("\r\n", "\r", "\n"), '<br>', $row[3]), $row[6], $row[0]);
        array_push($items, $item);
//        echo "<div class='pic-wrapper'>";
//         echo "<img src='$path' align='left' class='ms-pic col2' onclick='showFullPic(this, \"$id\", \"$is_liked\", \"$title\", \"$description\", \"$num_like\")'/>";
        //$a =  "<img src='" + $path + "' align='left' class='ms-pic col2' onclick='showFullPic(this," + "$title " + ", " + "$description" + ")'/>";
//        addBlueBevel(null, null);
//        echo "</div>";
    }
    return $items;
//     echo "</div>";
}

/*
function printFormForWorks()
{
echo <<<_FORM_FOR_WORKS
	<form method='post' action='profile.php' enctype='multipart/form-data'>
		<textarea  name='title' cols='25' rows='1' placeholder='Title' required></textarea>
		<textarea  name='detail' cols='50' rows='3' placeholder='Description'></textarea><br />
		<input type='file' name='image' size='14' maxlength='32' required/>
		<input type='hidden' name='upload_work'/>
		<input type='submit' value='Submit' />
	</form>
_FORM_FOR_WORKS;
}
*/

?>
