<?php
include_once '../common.php';

createTable('members',
            'user VARCHAR(16),
            pass VARCHAR(16),
            INDEX(user(6))');

createTable('messages', 
            'id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            auth VARCHAR(16),
            recip VARCHAR(16),
            pm CHAR(1),
            time INT UNSIGNED,
            message VARCHAR(4096),
            INDEX(auth(6)),
            INDEX(recip(6))');

createTable('friends',
            'user VARCHAR(16),
            friend VARCHAR(16),
            INDEX(user(6)),
            INDEX(friend(6))');


createTable('profiles',
            'user VARCHAR(64),
            gender BOOLEAN,
            birthday VARCHAR(64),
            work VARCHAR(64),
            college VARCHAR(64),
            interest VARCHAR(64),
            contact VARCHAR(64),
		    message VARCHAR(512),
            INDEX(user(6))');

createTable('pictures',
            'user VARCHAR(16),
            id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            title VARCHAR(128),
            detail VARCHAR(512),
            upload_time timestamp,
            path VARCHAR(128),
            num_good INT(128),
            INDEX(user(6))');

createTable('good',
            'id INT(15),
            user VARCHAR(16)');
            //INDEX(id(6))');
?>