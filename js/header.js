/*
* Depends on
* modal.js
* modal.css
*/


/****************************** Public ******************************/

/********** upload modal **********/

function showUploadModal()
{
	var removeFuncs = null;
	var callback = fadeoutModal.bind(null, 'modal-back-white-d', 'modal-upload-d', removeFuncs);
	fadeinModalBack('modal-back-white-d', callback);
	fadeinModalContent('modal-upload-d');
}


/****************************** Private ******************************/

/********** initialize **********/

$(function(){
	initializeDropImage();
	initializeSelectImage();
});

function initializeSelectImage()
{
	$("#upload-image-in").change(function(e){
		updateUploadWork(e);
	});
}

function updateUploadWork(e)
{
	var file = $("#upload-image-in")[0].files[0];
	if(file == null)
	{
		console.log("file is null");
		return;
	}
	if(file.type != 'image/jpeg' &&
	   file.type != 'image/pjpeg' &&
	   file.type != 'image/gif' &&
	   file.type != 'image/png')
	{
		printError("file type is not gif/jpeg/pjpeg/png");
		return;
	}

	var reader = new FileReader();
	reader.onerror = function(e) {};
	reader.onload = function(e) {
		loadWorkImage(e);
	};
	reader.readAsDataURL(file);
}

function initializeDropImage()
{
	var droppable = $(".droppable");
 
	if(!window.FileReader)
	{
		alert("FileAPI is not supported");
		return false;
	}
 
	var cancelEvent = function(event) {
		event.preventDefault();
		event.stopPropagation();
		return false;
	}

	droppable.bind("dragenter", cancelEvent);
	droppable.bind("dragover", cancelEvent);
 
	var file = function(e)
	{
		var file = event.originalEvent.dataTransfer.files[0];
 
		var reader = new FileReader();
		reader.onload = function(e) {
			loadImage(e);
		};
		reader.readAsDataURL(file);

	cancelEvent(event);
	return false;
	}
	droppable.bind("drop", file);
}

function loadWorkImage(e)
{
	var image = new Image();
	var width, height;
	image.src = e.target.result;
	image.onload = function(){
//		$('#upload-work-im').attr('src', image.src);
                var $td = $('#upload-pic-d').parent('td');
                $('#upload-pic-d').remove();
                $td.append("<img src='" + image.src + "' width='400' height='300' onclick='clickUploadImageIn()'/>");
	};
}

function clickUploadImageIn()
{
	$('#upload-image-in').click();
}

