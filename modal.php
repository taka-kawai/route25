<?php // modal.php

function addHiddenModal($main_id, $innerHTML)
{
echo <<<_ADD_MODAL_CONTENT
	<div id="$main_id">
		$innerHTML
	</div>
_ADD_MODAL_CONTENT;
}

function addHiddenLRModal($main_id, $left_id, $right_id)
{
echo <<<_ADD_LR_MODAL_CONTENT
	<div id="$main_id">
		<div id="$left_id">
		</div>
		<div id="$right_id">
		</div>
	</div>
_ADD_LR_MODAL_CONTENT;
}
