<?php // Example 21-12: logout.php
include_once 'session.php';
include_once 'common.php';
startSession($userstr, $user, $loggedin);

if (!$loggedin)
{
	echoDebug('not loged in');
	header("Location: ./login.php");
}

if (isset($_SESSION['user']))
{
	destroySession();
	header("Location: ./login.php");
}
else
{
		echo "<div class='main'><br />" .
				"You cannot log out because you are not logged in";
}
?>

<br /><br /></div></body></html>
