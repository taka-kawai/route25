<div class='common-d'>
	<div class='members-main'>
		<table id='members-table-ta'>
		{foreach from=$items item=item name=foo}
			{if $smarty.foreach.foo.index % $smarty.const.MEMBER_NUM_COLUMN == 0}
			<tr>
			{/if}
				<td class='members-td'>
					<a href='profile.php?view={$item[1]}'><img src='{$item[0]}' class='members-thumb-im clickable'/></a>
					<br />
					<div class='members-name-d'>{$item[1]}</div>
					{if (!$item[3])}
						<div id='{$item[1]}' class='members-{$item[2]}-d clickable' onclick='follow("{$item[1]}")'>{$item[2]}</div>
					{else}
    					<div id='{$item[1]}' class='members-unfollow-d clickable' onclick='unfollow("{$item[1]}")'>Unfollow</div>
    				{/if}
				</td>
			{if ($smarty.foreach.foo.index + 1) % $smarty.const.MEMBER_NUM_COLUMN == 0}
			</tr>
			{/if}
		{/foreach}
		</table>
	</div>
</div>