/* textSwitch.js
 *
 * Depends on
 * textSwitch.css
 * 
 */

$(function()
{
	initTextSwitch();

	$(".ts-default-text-d").click(function(e){
		startEdit($(this));
		e.stopPropagation();
	});

	$('.ts-editar-t').click(function(e){
		e.stopPropagation();
	});

	$(document).click(function(e){
		stopAllEdit();
	});
});

function initTextSwitch()
{
	$('.ts-editar-t').each(function(i, element)
	{
		var fadein = $($(element).parent('.ts-fadein-d'));
		var default_text = $(fadein.siblings('.ts-default-text-d'));
		var height = default_text.css('height');
		var width = default_text.css('width');
		console.log(height);

		$(element).css('height', height);
	});
}

function startEdit(me)
{
	var textSwitch = TextSwitchManager.getInstance().getTextSwitchById(me.attr("id"));
	textSwitch.startEdit(me);
}

function stopEdit(id)
{
	if (id != null) {
		var textSwitch = TextSwitchManager.getInstance().getTextSwitchById(id);
		textSwitch.stopEdit();
	}
}

function stopAllEdit()
{
	var list = TextSwitchManager.getInstance().getAllTextSwitch();
	for (var i = 0; i < list.length; i++)
	{
		stopEdit(list[i].getId());
	}
}

function stopEditByKeypress(code, id)
{
	if(code === 13)
	{
		stopEdit(id);
	}
}

var TextSwitchManager = (function() {
	var instance;
	var textSwitchList = [];

	function init() {
	     return {
		getTextSwitchById: function(id) {
			for(var i = 0; i < textSwitchList.length; i++)
			{
				if(textSwitchList[i].getId() == id)
				{
					return textSwitchList[i];
				}
			}
			var newInstance = new TextSwitch();
			newInstance.setId(id);
			textSwitchList.push(newInstance);
			return newInstance;		
		},
		getAllTextSwitch: function() {
			return textSwitchList;
		}
	     }
	}

	return {
		getInstance: function() {
			if(!instance) {
				instance = init();
			}
			return instance;
		}
	}
})();


function TextSwitch()
{

	/****************************** Public ******************************/

	this.startEdit = function(me)
	{
		switchToEditor(me);
	}

	this.stopEdit = function()
	{
		switchToDefault();
	}

	this.getId = function()
	{
		return instanceID;
	}

	this.setId = function(id)
	{
		instanceID = id;
	}

	/****************************** Private ******************************/

	var instanceID;
	var focused = null;

	function switchToEditor(me)
	{
		var fadein = $(me.siblings('.ts-fadein-d'));
		var editar = $(fadein.find('.ts-editar-t'));
		var text = $(me).text();
		if (text == "none") {
			text = "";
		}
		me.fadeOut("fast",function() {
			editar.val(text);
			fadein.fadeIn("fast");
			editar.focus();
		});

		focused = editar;

		console.log("focused is editar");
	}

	function switchToDefault()
	{
		if (focused == null) {
			return;
		}
		var editar = focused;
		var fadein = $(editar.parent('.ts-fadein-d'));
		var default_text = $(fadein.siblings('.ts-default-text-d'));
		var text = editar.val();
		fadein.fadeOut("fast",function() {
			default_text.text(text).fadeIn("fast");
			postText(text);
		});

		focused = null;
	}

	/*---------- private ----------*/

	function postText(text)
	{
        console.log(instanceID);
		var obj = {};
		obj[instanceID] = "";
		obj['text']	= text;
		$.post("profile.php", obj);
	}
}

