<?php /* discover.php */

/*-------------------- none UI --------------------*/

include_once 'session.php';
startSession($userstr, $user, $loggedin);

if (!$loggedin)
{
	header("Location: ./login.php");
}

/*-------------------- UI --------------------*/

include_once 'header.php';

includeDiscoverPHP();
includeDiscoverJS();
includeDiscoverCSS();
// echo "<div id='discovery-common-d'>";	
// echo "<div class='ms-main'>";
// showDiscoverTabs();
//showDiscover();
showDiscover($user);
// echo "</div>";
// echo "</div>";

closeHtml();

/*-------------------- functions --------------------*/

function includeDiscoverPHP()
{
	include_once 'config.php';
	include_once 'common.php';
	include_once 'modal.php';
	include_once 'like.php';
}

function includeDiscoverJS()
{
	echo "<script src='./js/discover.js'></script>";
	echo "<script src='./js/masonry.pkgd.min.js'></script>";
	echo "<script src='./js/masonry.js' ></script>";
	echo "<script src='./js/jquery.balloon.min.js' ></script>";
	echo "<script src='./js/balloon.js' ></script>";
	echo "<script src='./js/bevel.js'></script>";
	echo "<script src='./js/modal.js' ></script>";
	echo "<script src='./js/modalPicture.js' ></script>";
}

function includeDiscoverCSS()
{
	echo "<link rel='stylesheet' href='css/modal.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/modalPicture.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/masonry.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/bevel.css' type='text/css' />";
	echo "<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>";
	echo "<link rel='stylesheet' href='css/discover.css' type='text/css' />";
//	echo "<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>";
}

// function showDiscoverTabs()
// {
// echo <<<_DISCOVER_TAB
// 	<div id='discover-tab-d'>
// 		<table id='discover-tab-type-ta'>
// 			<tr><th class='discover-tab-th tab-default'>Picture</th><th class='discover-tab-th'>Craft</th><th class='discover-tab-th'>Movie</th></tr>
// 		</table>

// 		<table id='discover-tab-order-ta'>
// 			<tr><th class='discover-tab-th tab-default' onclick='updateDiscoverByTime(this)'>New</th><th class='discover-tab-th' onclick='updateDiscoverByLike(this)'>Popular</th></tr>
// 		</table>
			
// 	</div>
// 	<br />
// _DISCOVER_TAB;
// }

function showDiscover($user)
{
	$items = array();
	if (isset($_POST['discover']) && isset($_POST['order_by_like']))
	{
		$items = getItemListOrderByLike($user);
	}
	else
	{
		$items = getItemListOrderByTime($user);
	}
	
	$smarty = getSmarty();
	$smarty->assign('items', $items);
	$smarty->display('discover/main.tpl');
}

function getItemListOrderByTime($user)
{
    $pics = queryMysql("SELECT * FROM pictures order by upload_time desc");
    return getItemList($pics, $user);
}

function getItemListOrderByLike($user)
{
	$pics = queryMysql("SELECT * FROM pictures order by num_good desc");
	return getItemList($pics, $user);
}

function getItemList($pics, $user)
{
    $num_pics = mysql_num_rows($pics);

    if (!$num_pics)
    {
        echoError("mysql_num_rows returned 0");
        return;
    }

//     echo "<div id='discover-content-d' class='h-centered have-padding have-shadow'>";
//     echo "<div class='ms-container'>";

    $items = array();
    for($i = 0; $i < $num_pics; $i++)
    {
        $row = mysql_fetch_row($pics);
        $path = "$row[5]/$row[0]" . "_" . $row[1] . ".jpg";

//			   'user VARCHAR(16),
//             id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//             title VARCHAR(128),
//             detail VARCHAR(512),
//             upload_time timestamp,
//             path VARCHAR(128),
//             num_good INT(128),

        $isLiked = isLiked($row[1], $user);

        if (empty($row[2]) || !file_exists("$path"))
        {
            echoError("picture not found: $path");
            continue;
        }
        $item = array($path, $row[1], $isLiked, str_replace(array("\r\n", "\r", "\n"), '<br>', $row[2]),
				str_replace(array("\r\n", "\r", "\n"), '<br>', $row[3]), $row[6], $row[0]);
        array_push($items, $item);
//         echo "<img src='$path' align='left' class='ms-pic col2' onclick='showFullPic(this, \"$id\", \"$isLiked\", \"$title\", \"$description\", \"$num_like\")'/>";
    }
    return $items;
//     echo "</div>";
//     echo "</div>";
}

//     $num_pics = mysql_num_rows($pics);
//     if (!$num_pics)
//     {
//         echoError("[showDiscover] mysql_num_rows returned 0");
//         return;
//     }
//     echo "<div id='discover-content-d' class='h-centered have-padding have-shadow'>";
//     echo "<div class='ms-container'>";

//     for($i = 0; $i < $num_pics; $i++)
//     {
//         $row = mysql_fetch_row($pics);
//         echo $pics;
        
//         $id = $row[1];
//         $title = $row[2];
//         $description = $row[3];
//         $path = "$row[5]/$title";
//         $num_like = $row[6];
//         $isLiked = isLiked($id, $user);

//         if (!file_exists("$path"))
//         {
//             echoError("[showDiscover] picture not found");
//             continue;
//         }
//         echo "<img src='$path' align='left' class='ms-pic col2' onclick='showFullPic(this, \"$id\", \"$isLiked\", \"$title\", \"$description\", \"$num_like\")'/>";
//     }
//      echo "</div>";
//      echo "</div>";
// }

?>
