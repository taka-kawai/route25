<div id="modal-pic-d">
	<div id="modal-pic-left-d"></div>
	<div id="modal-pic-right-d">
		{if $isMe}
		<div class='bevel-blue' onclick='flipOverlay(this)'></div>
		<div id='item-flip-d'>
			<div class='front'>
			</div>
			<div class='back'>
				<span class='edit-item-s'>Title</span>
				<input type='text' id='edit-title-in' maxlength='32' name='update_title' placeholder='title' onBlur=''/><br>
				<span class='edit-item-s'>Description</span>
				<textarea id='edit-description-te' maxlength='512'
						wrap="soft" name='update_description' placeholder='description' onBlur=''></textarea><br>
				<form id='delete-f' method='post' action='profile.php' enctype='multipart/form-data'>
					<input id='delete-in' class='button-submit-default' type='submit' name='delete_work' value='Delete Picture' />
					<input id='delete-id-in' type='hidden' name='id' value=''/>
				</form>
				<button id='submit-bu' class='button-submit-default button-green'>Save</button>
			</div>
		</div>
		{/if}
	</div>
</div>