<?php

/*-------------------- main --------------------*/

/*
session_start();

include_once 'common.php';

$userstr = ' (Guest)';

if (isset($_SESSION['user']))
{
    $user     = $_SESSION['user'];
    $loggedin = TRUE;
    $userstr  = " ($user)";
}
else $loggedin = FALSE;
*/

/*-------------------- functions --------------------*/

//function startSession()
//{
//	if (session_status() == PHP_SESSION_NONE) {
//		session_start();
//	}
//}

function startSession(&$userstr, &$user, &$loggedin)
{
	static $doOnce = false;
	if ($doOnce == true) return;
	
	if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}

	$userstr = ' (Guest)';

	if (isset($_SESSION['user']))
	{
		$user     = $_SESSION['user'];
		$loggedin = TRUE;
		$userstr  = " ($user)";
	}
	else $loggedin = FALSE;
	
	$doOnce = true;
}

function destroySession()
{
	$_SESSION = array();

	if (session_id() != "" || isset($_COOKIE[session_name()]))
		setcookie(session_name(), '', time()-2592000, '/');

		session_destroy();
}
