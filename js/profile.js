/*
* Depends on
* modal.js
* modal.css
* 
*/

/****************************** Initialize ******************************/

$(function(){
	initialize();
});

function initialize()
{
	$("input[type=file]").on("click", function(e){
	   e.stopPropagation();
	})
}

/********** onClick **********/

function clickProfilePicIn(e)
{
	$('#profile-pic-in').click();
	//firefox doesn't work
//	var e = e || window.event;
	var e = arguments.callee.caller.arguments[0] || window.event;
	e.stopPropagation();
}

function clickCoverPicIn(e)
{
	$('#cover-pic-in').click();
}

function editProfile(me)
{
	changeBevelColor(me);
}

function changeBevelColor(bevel)
{
	$(bevel).css('background-color', 'rgba(78, 94, 114, 1)');
}

/****************************** Private ******************************/

/********** initialize **********/

var timer = false;
$(window).resize(function() {
    if (timer !== false) {
        clearTimeout(timer);
    }
    timer = setTimeout(function() {
        onResize();
    }, 200);
});

$(function(){
	initializeFriends();
	initializeMessageCard();
	initializeFrame();
	initializeBevel();
	initializeHover();
	initializeHeader();
	initializeCover();
	initializeFlip("#intro-flip-d");
	initializeFlip("#item-flip-d");
});

function onResize() {
	initializeFriends();
	initializeMessageCard();
	centerModalContent.bind(null, 'modal-profile-d');
}

function initializeHeader()
{
	$('#default-header').text('Profile');
}

function initializeHover()
{
	var thumb_height = $('#profile-pic-im').height();
	var thumb_width = $('#profile-pic-im').width();

	$('#thumb-hover-d').css('height', thumb_height);
	$('#thumb-hover-d').css('width', thumb_width);
}

function initializeMessageCard()
{
	calculateMessageCard();
}

function calculateMessageCard()
{
//	var name_height = $('#user-name').height();
//	var org_height = $('#user-name').siblings('.ts-textbox-d').height();
//
//	var inner_height = name_height + org_height;
//	console.log(inner_height);
//	$('#profile-name-org-inner-d').css('height', inner_height);

	var rightHeight = $('#profile-intro-right-d').height();
	var margin = parseInt($('#info-about-d').css('margin-top'), 10);
	$('#info-message-d').css('height', rightHeight - margin * 2);
}

function initializeFrame()
{
	var b_width = $('.frame-left-top').width();
	var b_height = $('.frame-left-top').height();
	var a_width = $('.frame-right-bottom').width();
	var a_height = $('.frame-right-bottom').height();

	$('.frame-left-top').css('left', b_width / 2 * -1);
	$('.frame-left-top').css('top', b_height / 2  * -1);
	$('.frame-right-bottom').css('right', a_width / 2 * -1);
	$('.frame-right-bottom').css('bottom', a_height / 2 * -1);
}

function initializeCover()
{
	$("#cover-pic-in").change(function(e){
		console.log("abc");
		$("#cover-pic-f").submit();
	});
}

function showEditIntro()
{
	var callback = fadeoutModal.bind(null, 'modal-back-black-d', 'modal-intro-d', removeThumbForm);
	fadeinModalBack('modal-back-black-d', callback);
	fadeinModalContent('modal-intro-d');
}

function showAbout()
{
	$("#info-about-d").show();
	$("#info-about-hidden-d").hide();
}

function showEditAbout()
{
//	$("#info-about-d").hide();
//	$("#info-about-hidden-d").show();
	
}

function resizeAboutHeight()
{
	var originalHeight = $("#info-about-d").outerHeight();
	var autoHeight = $("#info-about-d").css('height', 'auto').outerHeight();	

	$("#info-about-d .bevel-blue").remove();
	$("#info-about-d").prepend("<div class='bevel-blue' onclick='closeAbout(" + originalHeight + ")'></div>");

	$("#info-about-d").css('height', originalHeight).animate({height: autoHeight}, "fast", "linear");
}

function closeAbout(originalHeight)
{
	$("#info-about-d .bevel-blue").remove();
	$("#info-about-d").prepend("<div class='bevel-blue' onclick='openAbout()'></div>");
	$("#info-about-d").animate({height: originalHeight}, "fast", "linear");
}
