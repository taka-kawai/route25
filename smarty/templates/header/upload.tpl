<div id="modal-upload-d">
    <form id='upload-pic-f' method='post' action='profile.php' enctype='multipart/form-data'>
		<table border='0' width='500' cellspacing='0' cellpadding='10'>
            <tr>
                <td>
                    <div id='upload-pic-d' class='droppable' onclick='clickUploadImageIn()'>
                    	<i id='upload-icon-s' class='fi-upload-cloud' ></i>
                    </div>
                </td>
                <td align='left' nowrap>
                    <textarea id='upload-text-t' name='title' maxlength='32' cols='30' rows='2' placeholder='Title(Max:32 character)' required></textarea>
                    <textarea id='upload-detail-t' name='detail' maxlength='128' cols='30' rows='6' placeholder='Description(Max:128 character)'></textarea>
                    <input id='upload-image-in' type='file' name='image' size='14' maxlength='32' required/>
                    <input type='hidden' name='upload_work'/>
                    <input type='submit' id='upload-pic-su' class='button-submit-default' value='Upload' />
	        	</td>	
            </tr>
        </table>
    </form>
</div>