<?php // good.php

/*-------------------- none UI --------------------*/

includeGoodPHP();
include_once 'config.php';	

startSession($userstr, $user, $loggedin);

if (!$loggedin)
{
	header("Location: ./login.php");
}

updateGood($user);

/*-------------------- UI --------------------*/

includeGoodJS();
includeGoodCSS();

/*-------------------- functions --------------------*/

function includeGoodPHP()
{
	include_once 'session.php';
	include_once 'common.php';
}

function includeGoodJS()
{
	echo "<script src='./js/OSC.js'></script>";
	echo "<script src='./js/jquery-2.1.3.js'></script>";
	echo "<script src='./js/good.js'></script>";
}

function includeGoodCSS()
{
	echo "<link rel='stylesheet' href='css/Good.css' type='text/css' />";
}

function updateGood($user)
{
	if(isset($_POST['id']) && isset($_POST['increase']))
	{
		$id = intVal($_POST['id']);
		increaseGood($id, $user);
	}
	else if(isset($_POST['id']) && isset($_POST['decrease']))
	{
		$id = intVal($_POST['id']);
		decreaseGood($id, $user);
	}
}

function increaseGood($id, $user)
{
	if(!is_int($id) || $id < 0) return;

	$row = queryMysql("SELECT * FROM pictures where id ='$id'");
	
	if(mysql_num_rows($row) != 1)
	{
		echoError("mysql_num_rows returned " + mysql_num_rows($row));
		return;
	}
	$uniq = mysql_fetch_row($row);
	$num_good = $uniq[6] + 1;

	queryMysql("UPDATE pictures SET num_good ='$num_good' where id ='$id'");
	queryMysql("INSERT INTO good (id, user) VALUES ('$id', '$user')");   
}

function decreaseGood($id, $user)
{
	if(!is_int($id) || $id < 0) return;

	$row = queryMysql("SELECT * FROM pictures where id ='$id'");
	if(mysql_num_rows($row) != 1)
	{
		echoError("mysql_num_rows returned " + mysql_num_rows($row));
		return;
	}
	$uniq = mysql_fetch_row($row);
	$num_good = $uniq[6] - 1;
	queryMysql("UPDATE pictures SET num_good ='$num_good' where id ='$id'");
	queryMysql("DELETE FROM good where id='$id' AND user='$user'");   
}


