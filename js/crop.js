//mycrop.jp

/*
$(function()
{
    $('#jcrop-target-im').Jcrop({
        aspectRatio: 1,
        onSelect: updateCoords,
        onChange: showPreview
    });
});
*/

function registerCrop()
{
	$('#jcrop-target-im').Jcrop({
		aspectRatio: 1,
		setSelect:[100, 100, 300, 300],
		onSelect: updateCoords,
		onChange: showPreview
	});
//	$(".jcrop-holder").css('float', 'left');
}

function updateCoords(c)
{
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#w').val(c.w);
	$('#h').val(c.h);
}

function getRatio()
{
	var image = new Image();
	image.src = $("#jcrop-target-im")[0].src;

	return image.width / $("#jcrop-target-im").width();
}

function fixCoords()
{
	var ratio = getRatio();

	var original_xval = $('#x').val() * ratio;
	var original_yval = $('#y').val() * ratio;
	var original_wval = $('#w').val() * ratio;
	var original_hval = $('#h').val() * ratio;
	$('#x').val(original_xval);
	$('#y').val(original_yval);
	$('#w').val(original_wval);
	$('#h').val(original_hval);
}

function checkCoords()
{	
	if (parseInt($('#w').val())) return true;
	return false;
}

function showPreview(coords)
{
	var original_h = $("#jcrop-target-im").height();
	var original_w = $("#jcrop-target-im").width();
	var rx = 200 / coords.w;
	var ry = 200 / coords.h;
	$('#preview-im').css({
		width: Math.round(rx * original_w) + 'px',
		height: 'auto',
		marginLeft: '-' + Math.round(rx * coords.x) + 'px',
		marginTop: '-' + Math.round(ry * coords.y) + 'px'
	});
}
