<div id='modal-friends-d'>
 	<table id='modal-friends-table-ta'>
 		<span id='modal-friends-title-s'>Friends</span><br/>
 		{if count($friends) == 0 }
				<span id='modal-friends-nofriends-s'>Nothing to show.</span><br />
		{else}
			{foreach from=$friends item=friend name=foo}
				{if $smarty.foreach.foo.index % $smarty.const.MEMBER_NUM_COLUMN == 0}
				<tr>
				{/if}
					<td class='modal-friends-td'>
						<a href='profile.php?view={$friend}' class='modal-friends-thumbs-a'>
							<img src='./upload/profile/{$friend}_thumb.jpg' class='modal-friends-thumbs-im clickable'/>
						</a>
						<br />
						<div class='modal-friends-name-d'>{$friend}</div>
					</td>
				{if ($smarty.foreach.foo.index + 1) % $smarty.const.MEMBER_NUM_COLUMN == 0}
				</tr>
				{/if}
			{/foreach}
		{/if}
	</table>
</div>