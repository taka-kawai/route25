$(function(){
	initializeTrimModal();
});

function initializeTrimModal()
{
	$("#profile-pic-in").change(function(e){
		showProfileModal(e);
	});
}

function showProfileModal(e)
{
	console.log("showProfileModal");
	var file = $("#profile-pic-in")[0].files[0];
	if(file == null)
	{
		console.log("file is null");
		return;
	}
	if(file.type != 'image/jpeg' &&
	   file.type != 'image/pjpeg' &&
	   file.type != 'image/gif' &&
	   file.type != 'image/png')
	{
		printError("file type is not gif/jpeg/pjpeg/png");
		return;
	}

	var reader = new FileReader();
	reader.onerror = function(e) {};
	reader.onload = function(e) {
		loadProfileImage(e);
	};
	reader.readAsDataURL(file);
}

function loadProfileImage(e)
{
	var image = new Image();
	var width, height;
	image.src = e.target.result;
	image.onload = function(){
		width = this.width;
		height = this.height;
		appendThumbForm(image.src, width, height);
				
		var callback = fadeoutModal.bind(null, 'modal-back-black-d', 'modal-profile-d', removeThumbForm);
		fadeinModalBack('modal-back-black-d', callback);
		fadeinModalContent('modal-profile-d');
	};
}

function appendThumbForm(src, width, height)
{
	var max_width = $(window).width() * 0.8;
	var max_height = $(window).height() * 0.8;

	var resized = calculateFitSize(width, height, max_width, max_height);

	$("#modal-profile-left-d").prepend("<img src='" + src + "' id='jcrop-target-im' width='" + resized[0] + "' height='" + resized[1] + "'/>");
	$("#modal-profile-right-d").append("<div id='thumb-d' />");
	$("#thumb-d").append("<img src='" + src + "' id='preview-im'>");
//	$("#modal-profile-right-d").append("<input id='submit-button-in' type='button' value='submit' onclick='submitProfilePic()'/>");
	$("#modal-profile-right-d").append("<div id='upload-trimmed-d' onclick='submitProfilePic()'><i id='upload-trimmed-i' class='fi-arrow-up'></i></div>");
	registerCrop();
}

function removeThumbForm()
{
	$("#jcrop-target-im").remove();
	$(".jcrop-holder").remove();
	$("#thumb-d").remove();
	$("#upload-trimmed-d").remove();
	$("#intro-flip-d").flip(false);
}

function submitProfilePic()
{
	fixCoords();
	checkCoords();
	$("#profile-pic-f").submit();
}
