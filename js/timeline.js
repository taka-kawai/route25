$(function(){
	initializeHeader();
//	initializeBevel();
//	initializeFlip();
});

function initializeHeader()
{
	$('#default-header').text('Timeline');
}

function initializeFlip()
{
	$(".bevel-red").bind("click",function(){
		content_d = $($(this).parent());
		$("#timeline-content-d").removeClass('have-shadow');

		console.log(content_d);

		if(content_d.attr('type') == 'popular') revertContent(content_d);
		else if(content_d.attr('type') == 'latest') flipContent(content_d);
	});
	return false;
}

function flipContent(content_d)
{
	console.log('aaa');
	$(content_d).flip({
		direction:'lr',
		content: $(content_d).html(),
		color: '#FAFAF5',
		onEnd: function(){
			$(content_d).addClass('have-shadow');
			$(content_d).attr('type', 'popular');
			initializeFlip();
		}
	})
}

function revertContent(content_d)
{
	console.log('bbb');
	$(content_d).revertFlip();
	$(content_d).attr('type', 'latest');
}
