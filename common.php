<?php // common.php

/*-------------------- main --------------------*/

initializePHPError();
include_once 'config.php';

$dbhost  = '127.0.0.1';
$dbname  = 'route25';
$dbuser  = 'route25admin';
$dbpass  = 'route25qpalzmxncb';


mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
mysql_select_db($dbname) or die(mysql_error());

//openHtml();

includeCommonJS();
includeCommonCSS();

/*-------------------- functions --------------------*/

function initializePHPError()
{
	ini_set('display_errors',0);
	ini_set('log_errors','On');
	ini_set('error_log','/var/log/apache2/php_error.log');
}

function includeCommonPHP()
{
}

function includeCommonJS()
{
	echo "<script src='./js/OSC.js'></script>";
	echo "<script src='./js/jquery-2.1.3.js'></script>";
	echo "<script src='./js/common.js'></script>";
}

function includeCommonCSS()
{
	echo "<link rel='stylesheet' href='css/common.css' type='text/css' />";
}

function openHtml()
{
	static $doOnce = false;

	if($doOnce) return;
	echo "<!DOCTYPE html>\n<html>";
	echo "<head><title></title></head><body>";
	$doOnce = true;
}

function closeHtml()
{
	static $doOnce = false;

	if($doOnce) return;
	echo "<br /></body></html>";
	$doOnce = true;
}

function createTable($name, $query)
{
	queryMysql("CREATE TABLE IF NOT EXISTS $name($query)");
	echo "Table '$name' created or already exists.<br />";
}

function queryMysql($query)
{
	$result = mysql_query($query) or die(mysql_error());
	return $result;
}

function sanitizeString($var)
{
	$var = strip_tags($var);
	$var = htmlentities($var);
	$var = stripslashes($var);
	return mysql_real_escape_string($var);
}


function saveAsJpeg($img, $imgtype, $save_dir, $filename)
{
	if(!file_exists($save_dir) && !is_dir($save_dir))
	{
		mkdir($save_dir, 0777, TRUE);
		chmod($save_dir, 0777);
	}
	$ret = move_uploaded_file($img, "$save_dir/$filename");
	if($ret == FALSE)	echoError("failed to move to $save_dir");
	$src;
	$typeok = TRUE;
	switch($imgtype)
	{
		case "image/gif":   $src = imagecreatefromgif("$save_dir/$filename");break;
		case "image/jpeg":
		case "image/pjpeg": $src = imagecreatefromjpeg("$save_dir/$filename"); break;
		case "image/png":   $src = imagecreatefrompng("$save_dir/$filename"); break;
		default:            $typeok = FALSE; break;
	}
	if ($typeok == FALSE)
	{	
		echoError("file type is not gif/jpeg/pjpeg/png");
		return;
	}
	$ret = imagejpeg($src, "$save_dir/$filename.jpg");
	if($ret == FALSE) {
		echoError("imagejpeg failed");
	} else {
		unlink("$save_dir/$filename");
	}
}

function saveResizedImage($saveto)
{
	if (!isset($_FILES['image']['name']))
	{
		echoError("FILES not found");
		return;
	}
	move_uploaded_file($_FILES['image']['tmp_name'], $saveto);
	resizeImage($saveto, 300, $resized);
    
	$ret = imagejpeg($resized, $saveto);
	if($ret == FALSE) echoError("imagejpeg failed");
    
	imagedestroy($resized);
}

function resizeImage($saveto, $max, &$resized)
{
    $typeok = TRUE;
    switch($_FILES['image']['type'])
    {
        case "image/gif":   $src = imagecreatefromgif($saveto); break;
        case "image/jpeg":  // Both regular and progressive jpegs
        case "image/pjpeg": $src = imagecreatefromjpeg($saveto); break;
        case "image/png":   $src = imagecreatefrompng($saveto); break;
        default:            $typeok = FALSE; break;
    }
    
    if ($typeok == FALSE)
    {
        echoError("type is not ok");
        return;
    }
    list($w, $h) = getimagesize($saveto);

    $tw  = $w;
    $th  = $h;
        
    if ($w > $h && $max < $w)
    {
        $th = $max / $w * $h;
        $tw = $max;
    }
    elseif ($h > $w && $max < $h)
    {
        $tw = $max / $h * $w;
        $th = $max;
    }
    elseif ($max < $w)
    {
        $tw = $th = $max;
    }
    $resized = imagecreatetruecolor($tw, $th);
    imagecopyresampled($resized, $src, 0, 0, 0, 0, $tw, $th, $w, $h);
    imageconvolution($resized, array(array(-1, -1, -1),
        array(-1, 16, -1), array(-1, -1, -1)), 8, 0);
    if(!is_writable($saveto)){ echo "file not writable"; }
        
    //$ret = imagejpeg($tmp, $saveto);
    //imagedestroy($tmp);
    imagedestroy($src);
}

// function getProfileLink($view, $id, $class, $innerHTML)
// {
// 	return "<a href='profile.php?view=$view' class='$class' id='$id'>".
// 					$innerHTML .
// 	   		"</a>";
// }

function echoError($message)
{
	$dbg = debug_backtrace();
//	echo "[error:" . $dbg[1]['line'] . "][" . $dbg[1]['function'] . "] $message";
	$message = "[error:" . $dbg[0]['line'] . "][" . $dbg[1]['function'] . "] $message\n";
	//echo "<br></br>";
	$path = "./log/php_error.log";
	file_put_contents($path, $message, FILE_APPEND);
}

function echoDebug($message)
{
	$dbg = debug_backtrace();
	//echo "[debug:" . $dbg[1]['line'] . "][" . $dbg[1]['function'] . "] $message";
	$message = "[debug:" . $dbg[0]['line'] . "][" . $dbg[1]['function'] . "] $message\n";
	//echo "<br></br>";
	$path = "./log/php_error.log";
	file_put_contents($path, $message, FILE_APPEND);
}

function echoPost()
{
	foreach( $_POST as $key => $value ) {
		echoDebug(htmlspecialchars($key) . "=" . htmlspecialchars($value) . "<BR>");
	}
}
