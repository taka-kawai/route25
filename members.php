<?php // Example 21-9: members.php

include_once 'session.php';
startSession($userstr, $user, $loggedin);

if (!$loggedin) header("Location: ./login.php");

// echo "<div class='main'>";

// if (isset($_GET['view']))
// {
//     $view = sanitizeString($_GET['view']);
    
//     if ($view == $user) $name = "Your";
//     else                $name = "$view's";
    
//     echo "<h3>$name Profile</h3>";
// 	 echo "<a class='button' href='messages.php?view=$view'>" .
//          "View $name messages</a><br /><br />";

//     die("</div></body></html>");
// }
include_once 'header.php';

includeMembersCSS();
includeMembersJS();

if (isset($_GET['add']))
{
    $add = sanitizeString($_GET['add']);
    
    if (!mysql_num_rows(queryMysql("SELECT * FROM friends
        WHERE user='$add' AND friend='$user'")))
        queryMysql("INSERT INTO friends VALUES ('$add', '$user')");
}
elseif (isset($_GET['remove']))
{
    $remove = sanitizeString($_GET['remove']);
    queryMysql("DELETE FROM friends WHERE user='$remove' AND friend='$user'");
}

$result = queryMysql("SELECT user FROM members ORDER BY user");
$num    = mysql_num_rows($result);

// echo "<h3>Other Members</h3><ul>";
$items = array();
for ($j = 0 ; $j < $num ; ++$j)
{
    $row = mysql_fetch_row($result);
    if ($row[0] == $user) continue;
    
//     echo "<li><a href='members.php?view=$row[0]'>$row[0]</a>";
//     $follow = "follow";
// 	$item = "<a href='members.php?view=$row[0]'>$row[0]</a>";
	$name = $row[0];
	$action = "Follow";
	
    $following = mysql_num_rows(queryMysql("SELECT * FROM friends
        WHERE user='$row[0]' AND friend='$user'"));
    $followed = mysql_num_rows(queryMysql("SELECT * FROM friends
        WHERE user='$user' AND friend='$row[0]'"));

//     if (($t1 + $t2) > 1) echo " &harr; is a mutual friend";
//     elseif ($t1)         echo " &larr; you are following";
//     elseif ($t2)       { echo " &rarr; is following you";
// 	                      $follow = "recip"; }
    $status = "";
    if (($following + $followed) > 1) $status = " &harr; is a mutual friend";
    elseif ($following)         $status = " &larr; you are following";
    elseif ($followed)       { $status = " &rarr; is following you";
                      $action = "Confirm"; }
    
//     if (!$t1) echo " [<a href='members.php?add=".$row[0]    . "'>$follow</a>]";
//     else      echo " [<a href='members.php?remove=".$row[0] . "'>drop</a>]";
//     if (!$following) $extra = " <div id='$name" . "_' class='members-follow-d' onclick='follow($name)'> $action </div>";
//     else      $extra = " <div id='$name" . "' class='members-unfollow-d' onclick='unfollow($name)'>Unfollow</div>";

    $thumb = './upload/profile/' . $name . '_thumb.jpg';
    if (!file_exists($thumb)) {
		$thumb = './image/default/default_thumb.jpg';
    }
    $item = array($thumb, $name, $action, $following);
    array_push($items, $item);
}
$row_num = count($items) / MEMBER_NUM_COLUMN + 1;

$smarty = getSmarty();
$smarty->assign('row_num', $row_num);
$smarty->assign('items', $items);
$smarty->display('members/main.tpl');

function includeMembersJS()
{
	echo "<script src='./js/members.js'></script>";
}

function includeMembersCSS()
{
	echo "<link rel='stylesheet' href='css/members.css' type='text/css' />";
	echo "<link href='https://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>";
}
?>