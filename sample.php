<?php /* profile.php */

echo <<<EOT

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex, nofollow" />
	<title>jQuery Flip example</title>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='./js/jquery.flip.min.js'></script>
	<script>
    $(function(){
    	$(".flip-horizontal").flip({
  			trigger: 'hover'
		});
		$(".flip-vertical").flip({
			axis: 'x',
  			trigger: 'hover'
		});
    });
  	</script>
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css"/>
    <link href="./js/style.css" rel="stylesheet" media="all">
</head>
<body>

	<h2>Horizontal Flip</h2>
	
	<div id="flip-this" class="flip-horizontal"> 
  		<div class="front"> 
    		Front
  		</div> 
  		<div class="back">
    		<h3>Some Text in the Back!</h3>
  		</div> 
	</div>
	
	<h2>Vertical Flip</h2>
	
	<div id="flip-this" class="flip-vertical"> 
  		<div class="front"> 
    		z
  		</div> 
  		<div class="back">
    		<h3>Some Text in the Back!</h3>
  		</div> 
	</div>

</body>
</html>

EOT
?>
