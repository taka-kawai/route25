<?php /* profile-util.php */

function updateProfilePic($user)
{
	if(isset($_FILES['image']['name']) && isset($_POST['upload_profile_picture'])
	&& isset($_POST['x']) && isset($_POST['y']) && isset($_POST['w']) && isset($_POST['h']))
	{
		clearstatcache();
		$base_dir = "./upload/profile";
		$src = "$base_dir/$user.jpg";
		$dst = "$base_dir/$user" . "_thumb.jpg";
		
		echoDebug($dst);

		saveAsJpeg($_FILES['image']['tmp_name'], $_FILES['image']['type'], "$base_dir", "$user");
		saveThumbnail("$src", "$dst" , $_POST['x'], $_POST['y'], $_POST['w'], $_POST['h']);
	}
}

function updateCoverPic($user)
{
	if(isset($_FILES['image']['name']) && isset($_POST['upload_cover_picture']))
	{
		clearstatcache();
		$base_dir = "./upload/profile";
		$title = $user . "_cover";
		saveAsJpeg($_FILES['image']['tmp_name'], $_FILES['image']['type'], "$base_dir", "$title");
	}
}

function saveThumbnail($src, $dst, $x, $y, $w, $h)
{
	$t_width = $t_height = 100;
	$jpeg_quality = 90;

	$img_res = imagecreatefromjpeg($src);
	$dst_res = imagecreatetruecolor($t_width, $t_height);

	imagecopyresampled($dst_res, $img_res, 0, 0, $x, $y, $t_width, $t_height, $w, $h);

	//header('Content-type: image/jpeg');
	imagejpeg($dst_res, "$dst", $jpeg_quality);
	unlink($src);
}

function updateIntro($user)
{
	if (isset($_POST['birthday']))	updateMyTextSQL($user, 'birthday', $_POST['birthday']);
	if (isset($_POST['gender']))	updateMyTextSQL($user, 'gender', $_POST['gender']);
	if (isset($_POST['work']))		updateMyTextSQL($user, 'work', $_POST['work']);
	if (isset($_POST['college']))	updateMyTextSQL($user, 'college', $_POST['college']);
	if (isset($_POST['interest']))	updateMyTextSQL($user, 'interest', $_POST['interest']);
	if (isset($_POST['contact']))	updateMyTextSQL($user, 'contact', $_POST['contact']);
	if (isset($_POST['message']))	updateMyTextSQL($user, 'message', $_POST['message']);
}

function updateMyTextSQL($user, $column, $value)
{
	$value = preg_replace('/\s\s+/', ' ', sanitizeString($value));
	if (mysql_num_rows(queryMysql("SELECT * FROM profiles WHERE user='$user'")))
	{
		queryMysql("UPDATE profiles SET $column='$value' where user='$user'");
	} else {
		queryMysql("INSERT INTO profiles (user, $column) VALUES ('$user', '$value')");
	}
}

?>