<div id='profile-content-d' class='h-centered have-padding have-shadow'>
	<div class='ms-container'>
		{foreach from=$items item=item}
			<img src={$item[0]} align='left' class='ms-pic col2'
				onclick="showFullPic(this, '{$item[1]}', '{$item[2]}', '{$item[3]}', '{$item[4]}', '{$item[5]}',
					{if $isMe}
						'#item-flip-d > .front', '{$item[6]}')"/>
					{else}
						'#modal-pic-right-d', '{$item[6]}')"/>
					{/if}
		{/foreach}
	</div>
</div>