/* modalPicture.js
*
* Depends on
* modal.js
* modal.css
* balloon.js
"
*/

function showFullPic(obj, id, is_liked, title, description, num_like, selecter, username)
{
	var modal = getInstanceById(id, is_liked, num_like, obj, selecter);
	modal.showFullPic(title, description, username);
}

function increaseGood(id, num_like)
{
	var modal = getInstanceById(id);
	modal.increaseGood();
}

function decreaseGood(id, num_like)
{
	var modal = getInstanceById(id);
	modal.decreaseGood();
}

function updateDetail(id)
{
	var model = getInstanceById(id);
	model.updateDetail();
}

function getInstanceById(id, is_liked, num_liked, obj, selecter)
{
	if(typeof getInstanceById.Instances === 'undefined') {
		getInstanceById.Instances = [];
	}
	for(var i = 0; i < getInstanceById.Instances.length; i++)
	{
		if(getInstanceById.Instances[i].getId() == id) return getInstanceById.Instances[i];
	}
	var newInstance = new ModalPicture(id, is_liked, num_liked, obj, selecter);
	getInstanceById.Instances.push(newInstance);
	return newInstance;
}

function ModalPicture(id, is_liked, num_liked, obj, _selecter)
{
	var instanceID = id;
	var is_liked_local = is_liked;
	var num_like_local = parseInt(num_liked);
	var num_visit_local = parseInt(100);
	
	var instanceObj = obj;
	var selecter = _selecter;


	/****************************** Public ******************************/

	this.showFullPic = function(title, description, username)
	{
		var removeFuncs = function(){
			removeEdit();
			removeFullPic();
			removeTitle();
			removeUsername();
			removeDescription();
			removeLike();
			if (selecter == '#item-flip-d > .front') {
				$("#item-flip-d").flip(false);
			}
			$('#delete-id-in').val('');
		};
		var callback = fadeoutModal.bind(null, 'modal-back-white-d', 'modal-pic-d', removeFuncs);
		var img = new Image();
		img.src = instanceObj.src;

		appendFullPic(instanceObj.src, img.width, img.height);

		setRightSide();
		appendTitle(title);
		appendUsername(username);
		appendDescription(description);
		appendLike();
		appendEdit();
		fadeinModalBack('modal-back-white-d', callback);
		fadeinModalContent('modal-pic-d');
	}

	this.increaseGood = function()
	{
		num_like_local = num_like_local + 1;
		is_liked_local = true;

		replaceLikeRelated("decreaseGood", "modal-pic-cancel-like-i", "Cancel like");
		postIncreaseGood();
	}

	this.decreaseGood = function()
	{
		num_like_local = num_like_local - 1;
		is_liked_local = false;

		replaceLikeRelated("increaseGood", "modal-pic-like-i", "Like");
		postDecreaseGood();
	}
	
	this.updateDetail = function()
	{
		var title = $('#edit-title-in').val();
		var description = $('#edit-description-te').val();
		
		//update modal text
		$("#modal-pic-title").html(title);
		$("#modal-pic-description").html(description);
		
		//update showFullPic function
		$(instanceObj).attr('onclick', '');
		$(instanceObj).off('click');
		$(instanceObj).on('click', function() {
			showFullPic(obj, instanceID, is_liked_local, title, description, num_like_local, selecter);
		});
		
		////update DB
		postDetail(title, description);
	}

	this.getId = function()
	{
		return instanceID;
	}

	this.setId = function(id)
	{
		instanceID = id;
	}

	/****************************** Private ******************************/

	function setRightSide()
	{
		var width = $(window).width() * 0.25;
		$("#modal-pic-right-d").css("width", width);
	}

	function appendFullPic(src, width, height)
	{
		var max_width = $(window).width()*0.6;
		var max_height = $(window).height()*0.8;
		var resized = calculateFitSize(width, height, max_width, max_height);
		$("#modal-pic-left-d").prepend("<img src='" + src + "' id='full-pic' width='" + resized[0] + "' height='" + resized[1] + "'/>");	
	}

	function removeFullPic()
	{
		$('#full-pic').remove();
	}

	function appendTitle(title)
	{
//		$(selecter).append("<div id='modal-pic-title' class='edit_area'>" + title + "</div>");
		$(selecter).append("<div id='modal-pic-title'>" + title + "</div>");
		$("#edit-title-in").val(title);
	}

	function appendUsername(name) {
		$(selecter).append("<div id='modal-pic-username'>by <a id='modal-pic-userlink' href='profile.php?view=" + name +"'>" + name + "</a></div>");
	}

	function appendDescription(desc)
	{
//		$(selecter).append("<div id='modal-pic-description' class='edit_area' >" + desc + "</div>");
		$(selecter).append("<div id='modal-pic-description' class='pre-wrap'>" + desc + "</div>");
		$("#edit-description-te").val(desc);
	}

	function appendLike()
	{
		$(selecter).append("<div id='modal-pic-like-d'></div>");
		if(is_liked_local == true)
		{
			$("#modal-pic-like-d").append("<span id='modal-pic-like-s'><i id='modal-pic-cancel-like-i' class='fi-heart' alt='Cancel like' onclick='decreaseGood(" + instanceID + ")' /></span>");
		} else {
			$("#modal-pic-like-d").append("<span id='modal-pic-like-s'><i id='modal-pic-like-i' alt='Like' class='fi-heart' onclick='increaseGood(" + instanceID + ")' /></span>");
		}
//		$("#modal-pic-like-d").append("<span id='modal-pic-num-s'>" + num_like_local + " Likes / " + num_visit_local + " Views</span>");
		$("#modal-pic-like-d").append("<span id='modal-pic-num-s'>" + num_like_local + " Likes</span>");
		registerBalloon();
	}
	
	function appendEdit()
	{
		$('#submit-bu').on('click', function() {
			updateDetail(instanceID);
			if (selecter == '#item-flip-d > .front') {
				$("#item-flip-d").flip(false);
			}
		})
		$('#delete-id-in').val(instanceID);
		setDescriptionMaxHeight();
	}

	function removeEdit()
	{
		$('#submit-bu').off('click');
	}

	function setDescriptionMaxHeight()
	{
		var maxHeight = $("#modal-pic-d").height() -
			($(".edit-item-s").outerWidth(true) * 2 + $("#edit-title-in").outerWidth(true) +
			$("#delete-in").outerWidth(true));
		$("#edit-description-te").css('max-height', maxHeight);
	}	

	function replaceLikeRelated(onclick, id, balloon)
	{
		removeLike();
		$(selecter).append("<div id='modal-pic-like-d'></div>");
		$('#modal-pic-like-d').append("<span id='modal-pic-like-s'><i id='" + id + "' class='fi-heart' alt='" + balloon + "' onclick='" + onclick + "(" + instanceID + ", " + num_like_local + ")'/span>");
//		$(selecter).append("<span id='modal-pic-num-s'>" + num_like_local + " Likes / " + num_visit_local + " Views</span>");
		$('#modal-pic-like-d').append("<span id='modal-pic-num-s'>" + num_like_local + " Likes</span>");
		registerBalloon();
	}

	function removeTitle()
	{
		$('#modal-pic-title').remove();
		$("#edit-title-in").val('');
	}

	function removeUsername()
	{
		$('#modal-pic-username').remove();
	}

	function removeDescription()
	{
		$('#modal-pic-description').remove();
		$("#edit-description-te").val('');
	}

	function removeLike()
	{
		$(".balloon-like").remove();
		$('#modal-pic-like-d').remove();
		$('#modal-pic-like-s').remove();
		$('#modal-pic-num-s').remove();
	}

	function postIncreaseGood()
	{
		$.post("good.php", 
			{
			'id' : instanceID,
			'increase' : ""
			}
		);
	}

	function postDecreaseGood()
	{
		$.post("good.php", 
			{
			'id' : instanceID,
			'decrease' : ""
			}
		);
	}
	
	function postDetail(title, description)
	{
		var map = new Object();
		map['id'] = instanceID;
		map['update_title'] = title;
		map['update_description'] = description;
		$.post("update_pic.php", map);
	}

	//require balloon.js
	function registerBalloon()
	{
		registerLikeBalloon();
	}
}

/****************************** Not used ******************************/

function updateGood(user)
{
	$.post("is_liked.php", 
		{
			'id' : instanceID,
			'is_liked' : "",
			'user' : user
		}
	);	
}

