<?php /* header.h */

/*-------------------- none UI --------------------*/

include_once 'session.php';
startSession($userstr, $user, $loggedin);

/*-------------------- UI --------------------*/

//if (defined('HEADER_PHP_TEST')) openHtml();

includeHeaderPHP();
includeHeaderJS();
includeHeaderCSS();

openHtml();

$smarty = getSmarty();

if ($loggedin)
{
	$smarty->assign('user', $user);
	$smarty->display('header/user.tpl');
}
else
{
	$smarty->display('header/non_user.tpl');
}

/*
$innerHtml =
    "<form method='post' action='profile.php' enctype='multipart/form-data'>
        <table border='1' width='500' cellspacing='0' cellpadding='5'>
            <tr>
                <td>
                    <img id='upload-work-im' class='droppable' src='./image/header/upload.jpeg' width='460' height='320' onclick='clickUploadImageIn()'/>
                </td>
                <td align='left' nowrap>
                    <textarea id='upload-text-t' name='title' cols='25' rows='1' placeholder='Title' required></textarea>
                    <textarea id='upload-detail-t' name='detail' cols='50' rows='3' placeholder='Description'></textarea><br />
                    <input id='upload-image-in' type='file' name='image' size='14' maxlength='32' required/>
                    <input type='hidden' name='upload_work'/>
                    <input type='submit' value='Submit' />
	       </td>	
            </tr>
        </table>
    </form>";
*/

// $innerHtml =
//     "<form method='post' action='profile.php' enctype='multipart/form-data'>
//         <table border='1' width='500' cellspacing='0' cellpadding='5'>
//             <tr>
//                 <td>
//                     <div id='upload-pic-d' class='droppable' onclick='clickUploadImageIn()'>
//                     Click here and Post a Pic!
//                     </div>
//                 </td>
//                 <td align='left' nowrap>
//                     <textarea id='upload-text-t' name='title' cols='25' rows='1' placeholder='Title' required></textarea>
//                     <textarea id='upload-detail-t' name='detail' cols='50' rows='3' placeholder='Description'></textarea><br />
//                     <input id='upload-image-in' type='file' name='image' size='14' maxlength='32' required/>
//                     <input type='hidden' name='upload_work'/>
//                     <input type='submit' value='Submit' />
// 	       </td>	
//             </tr>
//         </table>
//     </form>";
// addHiddenModal('modal-upload-d', $innerHtml);


// if (defined('HEADER_PHP_TEST')) closeHtml();

/*-------------------- public API --------------------*/
/*
function showHeader($userstr, $user, $loggedin)
{
    include_once 'app_defs.php';
    include_once 'session.php';
    //include_once 'common.php';
    startSession($userstr, $user, $loggedin);

    openHtmlHeader();

    includeJS();
    includeCSS();

    echo "<title>$appname$userstr</title></head><body>";

    if ($loggedin)
    {
        echo "<div class='header'>";
        echo "<span id='logo'>$appname</span>";
        echo "<div id='default-header' class='abs-div-centered'>Default</div>";
    
        echo "<div id='hidden-header'>";
        echo "<a href='timeline.php?' class='menu_left'>Timeline</a>" .
                  "<a href='discover.php' class='menu_left'>Discover</a>" .
                  "<a href='friends.php' class='menu_left'>Friends</a>" .
                  "<a href='profile.php' class='menu_right'>Profile</a>" .
                  "<a href='logout.php' class='menu_right'>Log out</a></div>";
             echo "</div>";
//        echo "</div>";
    }
    else
    {
        echo "<div id='body-wrapper'>";
        echo "<div id='header-transparent' class='header'><span id='logo'>$appname</span>";
        echo "<a href='index.php?' class='menu_left'>Home</a>" .
             "<a href='signup.php' class='menu_left'>Sign up</a>" .
             "<a href='login.php' class='menu_left'>Log in</a></div><br/>";
    }
}
*/
/*-------------------- private API --------------------*/

function includeHeaderPHP()
{
	include_once 'config.php';
    include_once 'common_smarty.php';
    include_once 'common.php';
    include_once 'modal.php';
}

function includeHeaderJS()
{
//    echo "<script src='./js/OSC.js'></script>";
//    echo "<script src='./js/jquery-2.1.3.js'></script>";
//    echo "<script src='./js/common.js'></script>";
//    echo "<script src='./js/masonry.pkgd.min.js'></script>";
//    echo "<script src='./js/mymasonry.js' ></script>";
    echo "<script src='./js/modal.js'></script>";
    echo "<script src='./js/header.js'></script>";
//    echo "<script src='./js/jquery.Jcrop.min.js'></script>";
//    echo "<script src='./js/jquery-ui.min.js'></script>";
//    echo "<script src='./js/jquery.flip.min.js'></script>";
}

function includeHeaderCSS()
{
	echo "<link href='http://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>";
    echo "<link rel='stylesheet' href='css/header.css' type='text/css' />";
    echo "<link rel='stylesheet' href='css/modal.css' type='text/css' />";
    echo "<link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css'/>";
    echo "<link rel='stylesheet' href='css/jquery.Jcrop.min.css' type='text/css' />";
}

/*-------------------- public API --------------------*/

?>
