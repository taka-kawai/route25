<?php //login.php

/*-------------------- none UI --------------------*/

includeLoginPHP();
include_once 'session.php';

startSession($userstr, $user, $loggedin);
if($loggedin) {
	header("Location: ./profile.php");
	exit;
}

$error = $user = $pass = "";

if (isset($_POST['user']))
{
    $user = sanitizeString($_POST['user']);
    $pass = sanitizeString($_POST['pass']);
    if ($user == "" || $pass == "")
    {
        $error = "Not all fields were entered<br />";
    }
    else
    {
        $query = "SELECT user,pass FROM members
            WHERE user='$user' AND pass='$pass'";

        if (mysql_num_rows(queryMysql($query)) == 0)
        {
//            include_once 'header.php';
            $error = "<span class='error'>Username/Password
                      invalid</span><br /><br />";
        }
        else
        {
            $_SESSION['user'] = $user;
            $_SESSION['pass'] = $pass;
            header("Location: ./profile.php");     
        }
    }
}

/*-------------------- UI --------------------*/

include_once 'header.php';
//showHeader($userstr, $user, $loggedin);

includeLoginCSS();

$smarty = getSmarty();

$smarty->assign('user', $user);
$smarty->assign('pass', $pass);
$smarty->assign('error', $error);
$smarty->display('login/main.tpl');

// echo "<div class='main v-centered'>";
// showLoginForm($error, $user, $pass);

// echo "<br /></div>";
// closeHtml();

//close();

/*-------------------- private APIs --------------------*/

function includeLoginPHP()
{
	include_once 'config.php.php';
	include_once 'common.php';
}

function includeLoginCSS()
{
	echo "<link rel='stylesheet' href='css/common.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/login.css' type='text/css' />";
	echo "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300' type='text/css'>";
}

// function showLoginForm($error, $user, $pass)
// {
// echo <<<_END
// <form class='margin-left-2p' method='post' action='login.php'>$error
// 	<span class='fieldname'>User</span><input type='text'
// 	    maxlength='16' name='user' value='$user' /><br />
// 	<span class='fieldname'>Pass</span><input type='password'
// 	    maxlength='16' name='pass' value='$pass' />
// 	<br />
// 	<span class='fieldname'>&nbsp;</span>
// 	<input type='submit' value='Login' />
// </form>
// _END;
// }

// function close()
// {
// echo <<<_END
// <br />
// <span class='fieldname'>&nbsp;</span>
// <input type='submit' value='Login' />
// </form><br /></div></body></html>
// _END;
// }


?>	
