<div id="modal-intro-d">
	{if $isMe}
	<div class='bevel-blue' onclick='flipOverlay(this)'></div>
	{/if}
	<div id="intro-flip-d">
		<div class='front'>
			<span id='intro-message-s'>Message</span>
			<br />
			<span class='break-word pre-wrap' >{$about.message}</span><br/>
			<span id='intro-about-s'>About</span>
			<br />
			<span>Born on {$about.birthday}</span><br/>
			<span>
			{if $about.gender == 0}
			Male
			{elseif $about.gender == 1}
			Female
			{/if}
			</span><br/>
			<span>Works at {$about.work}</span><br/>
			<span>Graduated from {$about.college}</span><br/>
			<span>Interested in {$about.interest}</span><br/>
			<span>Contact me to {$about.contact}</span><br/>
		</div>
		<div class='back'>
			<form method='post' action='profile.php' enctype='multipart/form-data'>
				<span id='introduce-yourself-s'>Edit</span>
				<br />
				<span class='form-tab'>Message</span> <textarea id='edit-message-te' maxlength='512'
						wrap="soft" name='message' onBlur=''>{$about.message}</textarea><br>
				<!-- <span class='form-f'>Gender</span> 	<input type='text' maxlength='32' placeholder='{$about.gender}' name='gender' onBlur=''/><br> -->
				<span class='form-tab'>Work</span>	<input type='text' class='input-text-margintop' maxlength='64' value='{$about.work}' name='work' onBlur=''/><br>
				<span class='form-tab'>College</span>	<input type='text' class='input-text-margintop' maxlength='64' value='{$about.college}' name='college' onBlur=''/><br>
				<span class='form-tab'>Interested in</span> <input type='text' class='input-text-margintop' maxlength='64' value='{$about.interest}' name='interest' onBlur=''/><br>
				<span class='form-tab'>Contact</span> <input type='text' class='input-text-margintop' maxlength='64' value='{$about.contact}' name='contact' onBlur=''/><br>
				<input id='complete-in' class='button-green' type='submit' value='Save' />
			</form>
		</div>
	</div>
</div>