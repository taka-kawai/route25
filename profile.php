<?php /* profile.php */

/*-------------------- none UI --------------------*/

includeProfilePHP();
startSession($userstr, $user, $loggedin);

if (!$loggedin)
{
	header("Location: ./login.php");
	exit;
}

$view = getView($user);
// $text = getTextSQL($profile);
$isMe = false;
if ($view == $user)
{
	$isMe = true;
}

updateProfilePic($user);
updateCoverPic($user);
updateIntro($user);
updateWorks($user);

/*-------------------- UI --------------------*/

// if (defined('WORKS_PHP_TEST')) openHtml();

include_once 'header.php';
includeProfileJS();
includeProfileCSS();

// echo "<div id='profile-common-d'>";
// showProfile($view, $text);
// addHiddenLRModal('modal-profile-d', 'modal-profile-left-d','modal-profile-right-d');

// echo "<div class='ms-main clearfix'>";
// showLeftInfo($profile);
// showRightInfo($profile, $user);

// echo "</div>";

if (userExists($view)) {
	showProfile($view, $profile, $user, $isMe);
} else {
	showUserNotFound();
}

// include_once 'works.php';
// echo "</div>";



if (defined('WORKS_PHP_TEST')) closeHtml();

/*-------------------- functions --------------------*/

function includeProfilePHP()
{
	include_once 'config.php';
	include_once 'session.php';
	include_once 'common.php';
	include_once 'modal.php';
	include_once 'works.php';
	include_once 'friends.php';
	include_once 'profile-util.php';
}

function includeProfileJS()
{
	echo "<script src='./js/textswitch.js' ></script>";
	echo "<script src='./js/jquery.Jcrop.min.js'></script>";
	echo "<script src='./js/jquery.balloon.min.js' ></script>";
	echo "<script src='./js/balloon.js'></script>";
	echo "<script src='./js/crop.js'></script>";
	echo "<script src='./js/modal.js' ></script>";
	echo "<script src='./js/profile.js'></script>";
	echo "<script src='./js/cropProfile.js'></script>";
	echo "<script src='./js/bevel.js'></script>";
	echo "<script src='./js/jquery.flip.min.js'></script>";
	echo "<script src='./js/jquery.jeditable.mini.js'></script>";
	echo "<script src='./js/flip.js'></script>";
	echo "<script src='./js/modalFriends.js' ></script>";
}

function includeProfileCSS()
{
	echo "<link rel='stylesheet' href='css/profile.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/cropProfile.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/masonry.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/bevel.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/modal.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/init.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/jquery.Jcrop.min.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/modalFriends.css' type='text/css' />";
// 	echo "<link rel='stylesheet' href='foundation-icons.css' type='text/css' />";
	echo "<link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css'/>";
	echo "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300' type='text/css'>";
}

function userExists($user)
{
	$result = queryMysql("SELECT user FROM members");
	$num    = mysql_num_rows($result);

	for ($j = 0 ; $j < $num ; ++$j)
	{
		$row = mysql_fetch_row($result);
		if ($row[0] == $user) return true;
	}
	return false;
}

function getView($user)
{
	if (isset($_GET['view']))
	{
		return sanitizeString($_GET['view']);
	}
	else
	{
		return $user;
	}
}

function getProfileData($view)
{
	$row = null;
	$result = queryMysql("SELECT * FROM profiles WHERE user='$view'");
	if (!mysql_num_rows($result))
	{
		return "none";
	}
	$row = mysql_fetch_row($result);
	return $row;
}

function getTextSQL($row)
{
	return stripslashes($row[1]);
}

// function showProfilePic($view)
// {
// 	if (!file_exists("./upload/profile/$view" . "_thumb.jpg"))
// 	{
// 		echoError("profile file not found");
// 		return;
// 	}
// 	echo "<div id='profile-pic-d'>";
// 	echo "<img src='./upload/profile/$view" . "_thumb.jpg' id='profile-pic-im' class='.bevel-transparent-img ' align='left' onclick='clickProfilePicIn()'/>";
// 	echo "</div>";
// }

// function showProfile($view, $text)
// {
// 	echo "<div id='profile-background-pic-d'>";
	
// 	echo "<div id='profile-pic-and-name-d'>";
// 	showProfilePic($view);
	
// 	echo "<div id='profile-name-d'>";
	
// 	echo "<div id='user-name' >$view</div>";
// 	echo "</div>";
// 	echo "</div>";
// 	printFormForProfilePic();
// 	echo "</div>";
// }

function showProfile($view, $profile, $user, $isMe)
{
// 	clearstatcache();
	
	$about = getAboutList($view);
	$items = getWorksItemList($view, $user);
	$friends = getFriendsList($user);
	$thumb = './upload/profile/' . $view . '_thumb.jpg';
	if (!file_exists($thumb)) {
		$thumb = './image/default/default_thumb.jpg';
	}
	
	$smarty = getSmarty();
	$smarty->assign('isMe', $isMe);
	$smarty->assign('items', $items);
	$smarty->assign('view', $view);
	$smarty->assign('thumb', $thumb);
	$smarty->assign('about', $about);
	$smarty->assign('friends', $friends);
	$smarty->assign('date', date("YmdHis"));
	$smarty->display('profile/main.tpl');
}

function showUserNotFound()
{
	$smarty = getSmarty();
	$smarty->display('profile/user-not-found.tpl');
}


// function showLeftInfo($profile, $isMe)
// {
// 	echo "<div id='profile-info-left-d' class='profile-info-d'>";
// 	showAbout($profile, $isMe);
// 	addHiddenEditableAbout($profile);
// 	echo "</div>";
// }

// function showRightInfo($profile, $user)
// {
// 	echo "<div id='profile-info-right-d' class='profile-info-d'>";
// 	showMessage($profile);
// 	include_once 'friends.php';
// 	echo "</div>";
// }

function getAboutList($view)
{
// 	echo "<div id='info-about-d' class='profile-card-d'>";
// 	echo "<div class='bevel-blue' onclick='showEditAbout()'></div>";
// 	echo "<div class='card-title-d'>About</div>";
	$profile = getProfileData($view);
	
	$about = array();
	pushIfExist($about, 'gender', $profile[1]);
	pushIfExist($about, 'birthday', $profile[2]);
// 	pushIfExist($detailes, $age . " years old<br />");
	pushIfExist($about, 'work', $profile[3]);
	pushIfExist($about, 'college', $profile[4]);
//	echoItem("Graduated from", $profile[6], "none");
	pushIfExist($about, 'interest', $profile[5]);
	pushIfExist($about, 'contact', $profile[6]);
	pushIfExist($about, 'message', $profile[7]);
// 	echo "</div>";
	return $about;
}

function pushIfExist(&$array, $key, $value)
{
// 	if (!$value) {
// 		return;
// 	}
// 	$detaile = $value . "<br />";
// 	array_push($array, $detaile);
	$array += array($key=>$value);
}

function addHiddenEditableAbout($profile)
{
	echo "<div id='info-about-hidden-d' class='profile-card-d'>";
	echo "<div class='bevel-blue' onclick='showAbout()'></div>";
	echo "<div class='card-title-d'>About</div>";
	$age = 19;
	
	$class = "about-item-fixed";
	echo "<div class = '" . $class . "'>Born on " . $profile[3] . "</div>";
	echo "<div class = '" . $class . "'>" . $age . " years old</div>";
	
	$class = "about-item-editable";
	echoEditableItem("Gender ", $profile[2], "", $class, "sex");
	echoEditableItem("Work at ", 	$profile[4], "Your Company", $class, "work");
	echoEditableItem("Graduated from ", $profile[5], "Your School", $class, "college");
	//	echoItem("Graduated from", $profile[6], "none");
	echoEditableItem("Interested in ", 	$profile[7], "Your Hobby", $class, "interest");
	echoEditableItem("Contact me to ", 	$profile[8], "Email Adress", $class, "email");
	echo "</div>";
}

function echoEditableItem($item, $value, $default, $class, $id)
{
	$text = $value;
	if ($value == null)
	{
		$text = $default;
	}
	echo $item;
	echoTextSwitch($text, $class, $id);
	echo "<br />";
}

// function showMessage($profile)
// {
// 	echo "<div id='info-message-d' class='profile-card-d'>";
// 	echo "<div class='bevel-blue' onclick='editMessage(this)'></div>";
// 	echo "<div class='card-title-d'>Message</div>";
// 	echoEditMessage();
// 	echo "</div>";
// }

// function echoEditMessage($profile)
// {
// 	echoTextSwitch($profile[1], $class, $id);
// }


function echoTextSwitch($text, $class, $id)
{
echo <<<_PRINT_TEXT_BOX
	<span class='ts-textbox-d h-centered'>
		<span id='$id' class='ts-default-text-d $class'>$text</span>
		<span class='ts-fadein-d'>
			<input type='text' class='ts-editar-t' onkeypress='stopEdit(event.keyCode)'>
		</span>
	</span>
_PRINT_TEXT_BOX;
}

// function printFormForProfilePic()
// {
// echo <<<_PRINT_FORM_FOR_PROFILE
// 	<form method='post' id='profile-pic-f' action='profile.php' enctype='multipart/form-data'>
// 		<input type='file' id='profile-pic-in' class='input-file' name='image' size='14' maxlength='32'"/>
// 		<input type='hidden' name='upload_profile_picture' />
// 		<input type="hidden" id="x" name="x"/>
// 		<input type="hidden" id="y" name="y"/>
// 		<input type="hidden" id="w" name="w"/>
// 		<input type="hidden" id="h" name="h"/>
// 	</form>
	
// _PRINT_FORM_FOR_PROFILE;
// }

?>
