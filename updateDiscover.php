<?php /* iscover.php */

/*-------------------- none UI --------------------*/

//includeBasePHP();

//$user;

//startSession($userstr, $user, $loggedin);

//if (!$loggedin)
//{
//	header("Location: ./login.php");
//}

/*-------------------- UI --------------------*/

//include_once 'header.php';

//includeDiscoverJS();
//includeDiscoverCSS();

updateDiscover($user);

//closeHtml();

/*-------------------- functions --------------------*/

function includeBasePHP()
{
	include_once 'common.php';
	include_once 'session.php';
}

function includeBaseJS()
{
}

function includeDiscoverCSS()
{
}

function updateDiscover($user)
{
	if (isset($_POST['discover']) && isset($_POST['order_by_like']))
	{
		showDiscoverOrderByLike($user);
	}
	else
	{
		showDiscoverOrderByTime($user);
	}
}

function showDiscoverOrderByTime($user)
{
    $pics = queryMysql("SELECT * FROM pictures order by upload_time desc");
    $num_pics = mysql_num_rows($pics);

    if (!$num_pics)
    {
        echoError("[showDiscover] mysql_num_rows returned 0");
        return;
    }

    echo "<div id='discover-content-d' class='h-centered ms-container have-padding have-shadow'>";

    for($i = 0; $i < $num_pics; $i++)
    {
        $row = mysql_fetch_row($pics);
        $id = $row[1];
        $title = $row[2];
        $description = $row[3];
        $path = "$row[5]/$title";
        $num_like = $row[6];
        $isLiked = isLiked($id, $user);

        if (!file_exists("$path"))
        {
            echoError("[showDiscover] picture not found");
            continue;
        }
        echo "<img src='$path' align='left' class='ms-pic col2' onclick='showFullPic(this, \"$id\", \"$isLiked\", \"$title\", \"$description\", \"$num_like\")'/>";
    }
    echo "</div>";
}

function showDiscoverOrderByLike($user)
{
echoDebug("111");
    $pics = queryMysql("SELECT * FROM pictures order by num_good desc");
    $num_pics = mysql_num_rows($pics);
echoDebug("222");
    if (!$num_pics)
    {
        echoError("[showDiscover] mysql_num_rows returned 0");
        return;
    }
echoDebug("333");
    echo "<div id='discover-content-d' class='h-centered ms-container have-padding have-shadow'>";

    for($i = 0; $i < $num_pics; $i++)
    {
        $row = mysql_fetch_row($pics);
        $id = $row[1];
        $title = $row[2];
        $description = $row[3];
        $path = "$row[5]/$title";
        $num_like = $row[6];
        $isLiked = isLiked($id, $user);

        if (!file_exists("$path"))
        {
            echoError("[showDiscover] picture not found");
            continue;
        }
        echo "<img src='$path' align='left' class='ms-pic col2' onclick='showFullPic(this, \"$id\", \"$isLiked\", \"$title\", \"$description\", \"$num_like\")'/>";
    }
    echo "</div>";
}

?>
