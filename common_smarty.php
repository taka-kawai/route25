<?php // common.php

/*-------------------- functions --------------------*/

function getSmarty() {
	define('SMARTY_DIR', '/usr/local/lib/smarty-3.1.28/libs/');

	if (STAGE == 'LOCAL') {
		define('PROJECT_ROOT', '/home/taka/Documents/workspace/route25/Route25');
	} else if (STAGE == 'ALPHA_PRODUCTION') {
		define('PROJECT_ROOT', '/var/www/route25/route25');
	}
	require_once SMARTY_DIR . 'Smarty.class.php';
	
	$smarty = new Smarty();

	$smarty->setTemplateDir(PROJECT_ROOT . '/smarty/templates/');
	$smarty->setCompileDir(PROJECT_ROOT . '/smarty/templates_c/');
	$smarty->setConfigDir(PROJECT_ROOT . '/smarty/configs/');
	$smarty->setCacheDir(PROJECT_ROOT . '/smarty/cache/');
	
	return $smarty;
}
?>