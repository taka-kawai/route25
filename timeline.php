<?php /* timeline.php */

/*-------------------- none UI --------------------*/

includeTimelinePHP();
include_once 'config.php';

startSession($userstr, $user, $loggedin);

if (!$loggedin)
{
	header("Location: ./login.php");
}

/*-------------------- UI --------------------*/

include_once 'header.php';
//showHeader($userstr, $user, $loggedin);

includeTimelineJS();
includeTimelineCSS();

echo "<div id='timeline-common-d'>";

echo "<div class='ms-main'>";
showTimelineTabs();
showTimeline($user);
echo "</div>";
echo "</div>";

addHiddenLRModal('modal-pic-d', 'modal-pic-left-d', 'modal-pic-right-d');
closeHtml();

/*-------------------- private API --------------------*/

function includeTimelinePHP()
{
	include_once 'session.php';
	include_once 'common.php';
	include_once 'modal.php';
}

function includeTimelineJS()
{
//	echo "<script src='./js/bevel.js'></script>";
	echo "<script src='./js/timeline.js'></script>";
	echo "<script src='./js/masonry.pkgd.min.js'></script>";
	echo "<script src='./js/masonry.js' ></script>";
	echo "<script src='./js/modal.js' ></script>";
	echo "<script src='./js/modalPicture.js' ></script>";
}

function includeTimelineCSS()
{
	echo "<link rel='stylesheet' href='css/modal.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/modalPicture.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/masonry.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/bevel.css' type='text/css' />";
	echo "<link rel='stylesheet' href='css/timeline.css' type='text/css' />";
}

function showTimelineTabs()
{
echo <<<_TIMELINE_TAB
	<div id='timeline-tab-d'>
		<img class='clickable' width='40' height='40' src='image/dummy/heart.png' />
	</div>
	<br />
_TIMELINE_TAB;
}

function showTimeline($user)
{
    $follows = getFollow($user);
    $num_follows = mysql_num_rows($follows);
    if(!$num_follows)
    {
        echoError("[showTimeline] mysql_num_rows returned 0");
        return NULL;
    }

    for($i = 0; $i < $num_follows; $i++)
    {
        $row = mysql_fetch_row($follows);
        $follow_names[] = $row[0];
    }
//    $follows_pics = queryMysql("SELECT * FROM pictures WHERE user in ('" . implode("', '",$follow_names) . "');");
    $follows_pics = queryMysql("SELECT * FROM pictures WHERE user in ('" . implode("', '",$follow_names) . "') order by upload_time desc;");

    $num_pics = mysql_num_rows($follows_pics);

    echo "<div id='timeline-content-d' class='h-centered ms-container' type='latest'>";
//    echo "<div class='bevel-red' onclick='editProfile(this)'></div>";
//    echo "<div class='bevel-blue'></div>";
    for($i = 0; $i < $num_pics; $i++)
    {
        $row = mysql_fetch_row($follows_pics);
        $path = "$row[5]/$row[2]";

        if (!file_exists("$path"))
        {
            echoError("[showTimeline] picture not found");
            continue;
        }
        echo "<img src='$path' align='left' class='ms-pic col2' onclick='showFullPic(this)'/>";
    }
    echo "</div>";
}

function getFollow($target)
{
    //friend is follower in friends table
    $follows = queryMysql("SELECT user FROM friends WHERE friend='$target'");
    return $follows;
}
?>
