$(window).load(function(){
	initializeMasonry();
});


function initializeMasonry()
{
	var between = calculateGutter();
//	var between = 10;
	$('.ms-pic').css('margin-bottom', between);	
	$('.ms-container').masonry(
	{
		itemSelector: '.ms-pic',
		isAnimated: true,
		columnWidth: 0,
//		isFitWidth: true,
		gutter: between - 1 // To consider round up
	});
}

function calculateGutter()
{
	return ($(".ms-container").width() - ($('.ms-pic').width() * 3) ) / 2;
}


