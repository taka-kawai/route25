

function initializeBevel()
{
	$('.bevel-blue').each(function(){
		initializeRightTopBevel(this);
	});
}

function initializeRightTopBevel(bevel)
{
	$bevel = $(bevel);
	var width = $bevel.width();
	var height = $bevel.height();

	$bevel.css('top', height / 2  * -1);
	$bevel.css('right', width / 2 * -1);	
}
