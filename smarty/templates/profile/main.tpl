<div id='profile-common-d'>
	<div class='ms-main'>
		{include file='profile/cover.tpl' view=$view thumb=$thumb isMe=$isMe date=$date}
		{include file='profile/intro.tpl' about=$about friends=$friends}
		{include file='profile/items.tpl' items=$items isMe=$isMe}
	</div>
</div>
{include file='profile/overlay-item.tpl' isMe=$isMe}
{include file='profile/overlay-trim-profile-photo.tpl'}
{include file='profile/overlay-intro-full.tpl' about=$about isMe=$isMe}
{include file='profile/overlay-friends-full.tpl' friends=$friends}