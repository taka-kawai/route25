$(function(){
	$('.header').hover(function(){
		$('#default-header').stop().fadeTo('fast', 0, fadeinHiddenHeader());
	}, function(){
		$('#hidden-header').stop().fadeTo('fast', 0, fadeinDefaultHeader());
	});
});

function fadeinHiddenHeader()
{
	$('#hidden-header').fadeTo('fast', 1);
}

function fadeinDefaultHeader()
{
	$('#default-header').fadeTo('fast', 1);
}

function printDebug(message)
{
	console.log("[debug] " + message);
}

function printError(message)
{
	console.log("[error] " + message);
}
