/* mymodal.js */

/****************************** Public ******************************/

/****************************** Private ******************************/

/********** common **********/

function fadeinModalBack(back_id, click_callback)
{
	$("body").append("<div id='" + back_id + "'></div>");
	var modal = $("#" + back_id);

	modal.fadeIn("slow");
	modal.unbind().click(function(){
		if(click_callback == null) return;
		click_callback();
	});
}

function fadeinModalContent(content_id)
{
	centerModalContent(content_id);
	$("#" + content_id).fadeIn("slow");
}

function fadeoutModal(back_id, content_id, click_callback)
{
	$("#" + back_id + ", #" + content_id).fadeOut("slow",function(){
		$("#" + back_id).remove();
		if(click_callback == null) return;
		click_callback();
	});
}

function centerModalContent(content_id)
{
	var $content = $("#" + content_id);

	var w = $(window).width();
	var h = $(window).height();
	var cw = $content.outerWidth();
	var ch = $content.outerHeight();

	var pxleft = ((w - cw)/2);
	var pxtop = ((h - ch)/2);

	$content.css({"left": pxleft + "px"});
	$content.css({"top": pxtop + "px"});
	console.log('1111' . cw);
}

/********** util **********/

/*
* Calculate size to fit both max_width or max_height.
* Aspect will be retained.
*/

function calculateFitSize(width, height, max_width, max_height)
{
	var aspectWidth = width / max_width;
	var aspectHeight = height /max_height;

	var size;
	if(aspectWidth > aspectHeight) size = calculateFitWidth(width, height, max_width);
	else size = calculateFitHeight(width, height, max_height);

	return size;
}

function calculateFitWidth(width, height, max_width){
	var tw  = width;
	var th  = height;
	th = max_width / width * height;
	tw = max_width;

	var array = [tw, th];
	return array;	
}

function calculateFitHeight(width, height, max_height){
	var tw  = width;
	var th  = height;

	tw = max_height / height * width;
	th = max_height;

	var array = [tw, th];
	return array;
}

/****************************** ??? ******************************/

function fixContentWidth(id)
{
	var $content = $("#" + id);
	var $children = $($content.children());
	var total_width = 0;

	$children.each(function(){
		total_width += parseInt($(this).css('width'));
	});
	$content.css('width', 1400);
}

