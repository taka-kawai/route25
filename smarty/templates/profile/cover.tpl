{if $isMe}
<div id='profile-background-pic-d' style='background-image : url(../upload/profile/{$view}_cover.jpg?{$date})' onclick='clickCoverPicIn()'>
{else}
<div id='profile-background-pic-d' style='background-image : url(../upload/profile/{$view}_cover.jpg?{$date})' >
{/if}
	<div id='profile-pic-and-name-d'>
		<div id='profile-pic-d'>
			{if $isMe}
			<img src='{$thumb}?{$date}' id='profile-pic-im' class='.bevel-transparent-img ' align='left' onclick='clickProfilePicIn()'/>
			{else}
			<img src='{$thumb}?{$date}' id='profile-pic-no-pointer-im' class='.bevel-transparent-img ' align='left'/>
			{/if}
		</div>
		<div id='profile-name-d'>
			<div id='user-name' >{$view}</div>
		</div>
	</div>
</div>
<form method='post' id='profile-pic-f' action='profile.php' enctype='multipart/form-data'>
	<input type='file' id='profile-pic-in' class='input-file' name='image' size='14' maxlength='32'/>
	<input type='hidden' name='upload_profile_picture' />
	<input type="hidden" id="x" name="x"/>
	<input type="hidden" id="y" name="y"/>
	<input type="hidden" id="w" name="w"/>
	<input type="hidden" id="h" name="h"/>
</form>
<form method='post' id='cover-pic-f' action='profile.php' enctype='multipart/form-data'>
	<input type='file' id='cover-pic-in' class='input-file' name='image' size='14' maxlength='32'/>
	<input type='hidden' name='upload_cover_picture' />
</form>
