<?php //friends.php

/*-------------------- none UI --------------------*/

// include_once 'session.php';
// startSession($userstr, $user, $loggedin);

// if (!$loggedin)
// {
// 	header("Location: ./login.php");
// }

/*-------------------- UI --------------------*/

// include_once 'header.php';
includeFriendsPHP();
includeFriendsCSS();
includeFriendsJS();

// echoDebug($user);

// $view = $name1 = $name2 = $name3 = "";
// setUserName($view, $name1, $name2, $name3, $user);

// Uncomment this line if you wish the user�s profile to show here
// showProfile($view);

// $followers = array();
// $following = array();

// getFriends($mutual, $followers, $following, $view);

// $followers = array_diff($followers, $mutual);
// $following = array_diff($following, $mutual);
// $has_friends = FALSE;

// showFriends($mutual, $name2, $has_friends);
//showFollowers($mutual, $name2, $has_friends);
//showFollowing($mutual, $name3, $has_friends);

//echo "<a class='button' href='messages.php?view=$view'>" .
//     "View $name2 messages</a>";


/*-------------------- functions --------------------*/

function includeFriendsPHP()
{
	include_once 'common.php';
	include_once 'config.php';
}

function includeFriendsCSS()
{
	echo "<link rel='stylesheet' href='css/friends.css' type='text/css' />";
}

function includeFriendsJS()
{
	echo "<script src='./js/friends.js'></script>";
}

function setUserName(&$view, &$name1, &$name2, &$name3, $user)
{
	$view = getView($user);
	
	if ($view == $user)
	{
	    $name1 = $name2 = "Your";
	    $name3 =          "You are";
	}
	else
	{
	    $name1 = "<a href='members.php?view=$view'>$view</a>'s";
	    $name2 = "$view's";
	    $name3 = "$view is";
	}
}

function getFriendsList ($user) 
{
	$view = $name1 = $name2 = $name3 = "";
	setUserName($view, $name1, $name2, $name3, $user);
	
	// Uncomment this line if you wish the user�s profile to show here
	// showProfile($view);
	
	$followers = array();
	$following = array();
	
	getFriends($mutual, $followers, $following, $view);
	
	$followers = array_diff($followers, $mutual);
	$following = array_diff($following, $mutual);
	
	return $mutual;
	
// 	$list;
// 	if (sizeof($mutual))
// 	{
// // 		$list = getFriendsInfo($mutual);
// 		$list = $mutual;
// 	}
// 	else
// 	{
// 		$list = "Nothing to show.<br />";
// 	}
// 	echoDebug(sizeof($mutual));
	
// 	return $list;
}

function getFriends(&$mutual, &$followers, &$following, $view)
{
	$result = queryMysql("SELECT * FROM friends WHERE user='$view'");
	$num    = mysql_num_rows($result); 

	for ($j = 0 ; $j < $num ; ++$j)
	{
	    $row           = mysql_fetch_row($result);
	    $followers[$j] = $row[1];
	}

	$result = queryMysql("SELECT * FROM friends WHERE friend='$view'");
	$num    = mysql_num_rows($result);

	for ($j = 0 ; $j < $num ; ++$j)
	{
	    $row           = mysql_fetch_row($result);
	    $following[$j] = $row[0];
	}

	$mutual = array_intersect($followers, $following);
}

function getFriendsInfo($mutual)
{
	$friends_info = array();

	foreach($mutual as $friend)
	{
		if ($friend == null) continue;
		$innerHTML = "<img src='./upload/profile/$friend" . "_thumb.jpg' class='friends-thumbs-im clickable'/>";
		$info = array(getProfileLink($friend, null, 'friends-thumbs-a', $innerHTML), $friend);
		array_push($friends_info, $info);
	}
	return $friends_info;
}

function showFollowers($mutual, $name2, &$has_friends)
{
	if (sizeof($followers))
	{
	    echo "<span class='subhead'>$name2 followers</span><ul>";
	    foreach($followers as $friend)
	        echo "<li><a href='members.php?view=$friend'>$friend</a>";
	    echo "</ul>";
	    $has_friends = TRUE;
	}
}

function showFollowing($mutual, $name3, &$has_friends)
{
	if (sizeof($following))
	{
	    echo "<span class='subhead'>$name3 following</span><ul>";
	    foreach($following as $friend)
	        echo "<li><a href='members.php?view=$friend'>$friend</a>";
	    echo "</ul>";
	    $has_friends = TRUE;
	}
}
?>

