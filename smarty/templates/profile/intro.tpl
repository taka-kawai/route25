<div id='profile-intro-d' class='clearfix'>
	<div id='profile-intro-left-d' class='profile-intro-common' onclick='showEditIntro()'>
		<div id='info-message-d' class='profile-card-d clickable'>
<!-- 			<div class='bevel-blue' onclick='editMessage(this)'></div> -->
			<div class='card-title-d'>Message</div>
				<div class='break-word pre-wrap'>{$about.message}</div>
		</div>
	</div>
	<div id='profile-intro-right-d' class='profile-intro-common'>
		<div id='info-about-d' class='profile-card-d clickable' onclick='showEditIntro()'>
<!-- 			<div class='bevel-blue' onclick='showEditAbout()'></div> -->
			<div class='card-title-d'>About</div>
				Birthday : {$about.birthday}<br/>
				{if $about.gender == 0}
				Male
				{elseif $about.gender == 1}
				Female
				{/if}
		</div>
		<div id='info-friends-d' class='profile-card-d clickable'>
			<div class='card-title-d' onclick='showFriendsList()' >Friends</div>
			{if count($friends) == 0 }
				Nothing to show.<br />
			{else}
				{foreach from=$friends item=friend name=foo}
					{if $smarty.foreach.foo.index == 5}
						{break}
					{/if}
					<a href='profile.php?view={$friend}' class='friends-thumbs-a'>
						<img src='./upload/profile/{$friend}_thumb.jpg' class='friends-thumbs-im clickable'/>
					</a>
				{/foreach}
			{/if}
		</div>
	</div>
</div>