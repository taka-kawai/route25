$(function(){
	initializeHeader();
	initializeBevel();
});

function initializeHeader()
{
	$('#default-header').text('Discover');
}

function filterDiscoverOnlyPicture()
{
	colorBackground(me);
}

function filterDiscoverOnlyCraft()
{
}

function filterDiscoverOnlyMovie()
{
}

function updateDiscoverByTime(me)
{
	colorBackground(me);
	loadOrderByTime();
}

function updateDiscoverByLike(me)
{
	colorBackground(me);
	loadOrderByLike();
}

function colorBackground(me)
{
	$(me).css("background-color", "#BAD2BD")
	     .css("color", "#FFF");
	$("#discover-tab-order-ta .discover-tab-th").not(me)
		.css("background-color", "transparent")
		.css("color", "#B0ADAD");
}

function loadOrderByTime()
{
	$("#discover-content-d").load("discover.php .ms-container",
		{
		'discover' : "",
		'order_by_time' : ""
		},
		initializeMasonry
	);
}

function loadOrderByLike()
{
	$("#discover-content-d").load("discover.php .ms-container",
		{
		'discover' : "",
		'order_by_like' : ""
		},
		initializeMasonry
	);
}
