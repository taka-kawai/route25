<?php //signup.php

include_once 'session.php';
// startSession($userstr, $user, $loggedin);

includeSignUpCSS();
includeSignUpPHP();
includeSignUpJS();

startSession($userstr, $user, $loggedin);
if($loggedin) {
	header("Location: ./profile.php");
	exit;
}

$error = $user = $userstr = $pass = "";
if (isset($_SESSION['user'])) destroySession();

if (isset($_POST['user']))
{
    $user = sanitizeString($_POST['user']);
    $pass = sanitizeString($_POST['pass']);

    if ($user == "" || $pass == "") {
        $error = "Not all fields are entered<br /><br />";
    }
    else
    {
        if (mysql_num_rows(queryMysql("SELECT * FROM members WHERE user='$user'")))
            $error = "That username already exists<br /><br />";
        else
		{
        	queryMysql("INSERT INTO members VALUES('$user', '$pass')");
            //queryMysql("INSERT INTO profiles VALUES('$user', '$pass')");
//             die("<h4>Account created</h4>Please Log in.<br /><br />");

 			startSession($userstr, $user, $loggedin);
        	$_SESSION['user'] = $user;
        	$_SESSION['pass'] = $pass;
        	header("Location: ./init.php");
        }
    }
}

include_once 'header.php';

$smarty = getSmarty();

$smarty->assign('user', $user);
$smarty->assign('pass', $pass);
$smarty->assign('error', $error);
$smarty->display('signup/main.tpl');

/*-------------------- functions --------------------*/

function includeSignUpCSS()
{
	echo "<link rel='stylesheet' href='css/common.css' type='text/css' />";
	echo "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300' type='text/css'>";
}

function includeSignUpJS()
{
	echo "<script src='./js/signup.js'></script>";
}

function includeSignUpPHP()
{
	include_once 'common.php';
	include_once 'common_smarty.php';
}

?>