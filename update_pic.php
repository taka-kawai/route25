<?php // UpdatePic.php

/*-------------------- none UI --------------------*/

includeUpdatePicPHP();
include_once 'config.php';	

startSession($userstr, $user, $loggedin);

if (!$loggedin)
{
	header("Location: ./login.php");
}

updateDetail($user);

/*-------------------- UI --------------------*/

includeUpdatePicJS();
includeUpdatePicCSS();

/*-------------------- functions --------------------*/

function includeUpdatePicPHP()
{
	include_once 'session.php';
	include_once 'common.php';
}

function includeUpdatePicJS()
{
	echo "<script src='./js/OSC.js'></script>";
	echo "<script src='./js/jquery-2.1.3.js'></script>";
	echo "<script src='./js/update_pic.js'></script>";
}

function includeUpdatePicCSS()
{
//	echo "<link rel='stylesheet' href='css/UpdatePic.css' type='text/css' />";
}

function updateDetail($user)
{
	if(isset($_POST['id']) && isset($_POST['update_title']))
	{
		$id = intVal($_POST['id']);
		updateTitle($id, $user, sanitizeString($_POST['update_title']));
	}
	if(isset($_POST['id']) && isset($_POST['update_description']))
	{
		$id = intVal($_POST['id']);
		updateDescription($id, $user, sanitizeString($_POST['update_description']));
	}
}

function updateTitle($id, $user, $title)
{
	if(!is_int($id) || $id < 0) return;
	queryMysql("UPDATE pictures SET title ='$title' where id ='$id'");
}

function updateDescription($id, $user, $description)
{
	if(!is_int($id) || $id < 0) return;
	queryMysql("UPDATE pictures SET detail ='$description' where id ='$id'");
}


